<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define ("DB_Host" , '127.0.0.1');
define ("DB_Name" , 'taskmanager');
define ("DB_User" , 'root');
define ("DB_Password" , '');
define ("Task_Table" , 'tasks');
define ("Enum_Column_Owner" , 'owner');

$conn = new mysqli(DB_Host , DB_User , DB_Password , DB_Name);

if ($conn->connect_error)
{
    die("Database connection error :" . $conn->connect_error);
}

$task_statuses = [1=>'Draft',2=>'Todo' , 3=>'Done' , 4=>'Archive'];

if(isset($_GET['del']))
{
    delete_task($_GET['del']);
    call_current_page();
}

$where_condition = '';
if(isset($_GET['status']))
{
    $where_condition = ' where status='.$_GET['status'];
}

$tasks = get_tasks($where_condition);

//$conn->close();
?>

