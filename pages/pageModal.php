<div class="modal fade" id="taskModal" tabindex="-1" role="dialog" aria-labelledby="taskModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">New Task</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="modalForm" action="pages\ajaxProcess.php">
          <div class="form-group">
            <label for="modalTaskTitle" class="col-form-label">Title:</label>
            <input type="text" class="form-control" id="modalTaskTitle" name="modalTaskTitle">
          </div>
          <div class="form-group">
            <label for="modalTaskOwner">Owner</label>
              <?=enumDropdown(Task_Table, Enum_Column_Owner, false,"modalTaskOwner");?>
          </div>
          <div class="form-group">
            <label for="modalTaskStatus">Task Status</label>
              <select name="modalTaskStatus" id="modalTaskStatus">
                <option value="1">Draft</option>
                <option value="2">Todo</option>
                <option value="3">Done</option>
                <option value="4">Archive</option>
              </select>
          </div>
          <input type="hidden" id="modalActionType" name="modalActionType" value="Add">
          <input type="hidden" id="modalTaskId" name="modalTaskId" value="0">
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" id="submitModal" name="submitModal" class="btn btn-primary" value="Add">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>